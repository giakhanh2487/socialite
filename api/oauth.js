import 'isomorphic-fetch';

import debug from 'debug';
import fs from 'fs';
import path from 'path';
import route from 'koa-route';

import jwt from 'jsonwebtoken';
import { Facebook } from 'fb';

const PROVIDER_FACEBOOK = 'facebook';
const dbg = debug('socialite:oauth');

const OAUTH_REDIRECT_BASE_URI = process.env.OAUTH_REDIRECT_BASE_URI;
const JWT_SECRET = process.env.JWT_SECRET;
let friends = [];

export default function (app, { provider, appId, appSecret }) {

  if (provider === PROVIDER_FACEBOOK) {
    dbg('OAuth2 provider: Facebook');
    app.use(authenticate(appId, appSecret));
    app.use(route.get('/auth/facebook', auth(appId, appSecret)));
  }
}

function auth (appId, appSecret) {
  const redirectUri = `${OAUTH_REDIRECT_BASE_URI}/auth/facebook`;
  dbg(`Using redirectUri = ${redirectUri}`);

  return async function (ctx) {
    let { code } = ctx.request.query;
    dbg(`Using redirectUri = ${redirectUri}`);
    const res = await fetch(
        `https://graph.facebook.com/v3.0/oauth/access_token?`
        + `client_id=${appId}`
        + `&redirect_uri=${redirectUri}`
        + `&client_secret=${appSecret}`
        + `&code=${code}`);
    const { status, body } = res;
    if (status !== 200) {
      dbg(`FB Oauth returned status ${status}`);
      if (body && body._buffer) {
        const error = body._buffer.toString().substring(0, 80);
        dbg('FB Response', error);
      }

      return ctx.throw(403, 'Invalid access');
    }

    const {
      access_token,
      token_type,
      expires_in
    } = await res.json();


    const fb = new Facebook({appId, appSecret, version: 'v3.0' });
    const fbResponse = await fb.api('me', {access_token, fields: ['id', 'name']});

    dbg('Facebook response:', fbResponse);

    if (!isFriend(fbResponse)) {
      ctx.throw(403, 'Invalid access');
    }
    const { id, id: uid, name } = fbResponse;
    global.friendCache.set(id, { id, name });

    const token = jwt.sign({ uid, name }, JWT_SECRET, { expiresIn: expires_in });

    ctx.status = 200;
    ctx.body = `
    <p>Redirecting...</p>
    <script>
      (function() {
        var main = window.opener;
        if (!main) return;
        main.postMessage("${token}", "*");
        setTimeout(function(){ window.close(); }, 200);
      })() 
    </script>`;
  }
}

function authenticate (appId, appSecret) {
  if (process.env.FRIEND_LIST && fs.existsSync(process.env.FRIEND_LIST)) {
    console.log(`Friend database ${process.env.FRIEND_LIST}`)
    let lines = fs.readFileSync(process.env.FRIEND_LIST, 'utf8');
    friends = lines.split(/\n/);

    dbg(`Socialite will authorize up to ${friends.length} friends`);
  } else if (process.env.FRIEND_LIST) {
    dbg(`Cannot find friend list at ${process.env.FRIEND_LIST}`);
  }

  return async function(ctx, next) {
    console.log('ctx.request.path:', ctx.request.path);
    if (ctx.request.path.indexOf('/auth/facebook') === 0) {
      await next();
      return;
    }
    if (ctx.request.path.indexOf('/favicon.ico') === 0) {
      await next();
      return;
    }

    const authorization = ctx.request.headers['authorization'];
    dbg('authorization:', authorization);
    if (authorization) {
      const [ , token ] = authorization.split('Bearer ');
      let decoded;
      if (decoded = jwt.verify(token, JWT_SECRET)) {
        ctx.state.oauth = {
          id: decoded.uid
        };
      } else {
        ctx.throw(403, 'Invalid access. JWT failed.');
      }
    } else {
      ctx.throw(401, 'Authentication required');
    }

    await next();
  };
}

function isFriend({ id, name }) {
  return friends.includes(name);
}