import debug from 'debug';

import Koa from 'koa';
import cors from 'koa-cors';
import koaBody from 'koa-body';
import bodyParser from 'koa-bodyparser';
import { connect } from './db';
import oauth from './oauth';
import pnc from './pnc';
import media from './media';
import NodeCache from 'node-cache';

global.friendCache = new NodeCache();
const app = new Koa();
const dbg = debug('socialite');

Object.keys(process.env).filter(it => it.toUpperCase() === it).forEach(it => dbg(`${it} = ${process.env[it]}`));

app.use(cors({
  origin: process.env.CORS || '*',
  methods: 'GET,PUT,POST',
}));

app.use(async (ctx, next) => {
  try {
    dbg('[REQUEST HEADERS]\n', ctx.req.headers, '\n');
    return await next();
  } catch (e) {
    dbg('Exception:', e);

    ctx.status = e.status;
    ctx.body = e.message;
  }
});
app.use(koaBody({ multipart: true }));
app.use(bodyParser());
app.use(connect(process.env.DATABASE_URL));
oauth(app, {
  provider: 'facebook',
  appId: process.env.FB_APP_ID,
  appSecret: process.env.FB_APP_SECRET
});

pnc(app, '/api');
media(app, '/api');

app.listen(8080, () => console.log('Serving at port 8080'));
