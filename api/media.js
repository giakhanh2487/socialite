// PnC :: Post and Comment
import 'isomorphic-fetch';
import debug from 'debug';

import fs from 'fs';
import path from 'path';
import { v4 as uuidV4 } from 'uuid';

import route from 'koa-route';
import Hashids from 'hashids';

import { Dropbox } from 'dropbox';

const dbg = debug('socialite:media');
const hashids = new Hashids(process.env.MEDIA_SALT);

const MEDIA_COUNT_PER_POST = 5;
let dropbox;

function uniqueMediaKey() {
  const buffer = new Array();
  uuidV4(0, buffer, 16);

  return hashids.encode(buffer.filter(Boolean));
}

async function createMedia(files) {
  dbg('createMedia')

  if (!files.media) {
    return {
      filenames: []
    };
  }

  const media = Array.isArray(files.media) ? files.media.slice(0, MEDIA_COUNT_PER_POST) : [files.media];

  try {
    let filenames = [];

    for (let mediaItem of media) {
      let tmpFilename = `${uniqueMediaKey()}${path.extname(mediaItem.name)}`;
      let tmpFilePath = `/${tmpFilename}`;
      dbg(`Temp file: ${tmpFilePath} for uploaded: ${mediaItem.path}`);

      await new Promise((resolve, reject) => {
        fs.readFile(mediaItem.path, (err, contents) => {
          if (err) {
            return reject(err);
          }

          dropbox.filesUpload({ path: tmpFilePath, contents: contents }).then(response => {
            dbg(`Done writing ${tmpFilePath}`);
            try { fs.unlinkSync(mediaItem.path); } finally {}

            resolve();
          }).catch(reject);
        });
      });

      filenames.push(tmpFilename);
    }
    return {
      filenames
    };
  } catch (error) {
    dbg('Error while uploading:', error);

    return {
      error
    };
  };
}

export default function (app, baseUrl) {
  dropbox = new Dropbox({ accessToken: process.env.DROPBOX_ACCESS_TOKEN });

  dbg('Mounting file handlers...');
  app.use(route.post(`${baseUrl}/media/upload`, async function (ctx) {
    ctx.body = await createMedia(ctx.request.files);
  }));
}