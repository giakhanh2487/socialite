import sanitizeHtml from 'sanitize-html';

export const RULE_CHARS_LIMIT = 500;
export const RULE_FILE_UPLOAD_LIMIT = 5;

export const ERROR_EMPTY_VIOL = 'empty';
export const ERROR_EMPTY_MSG = 'Your post is empty';

export const ERROR_CONTENT_VIOL = 'content';
export const ERROR_CONTENT_MSG = 'Your post contains errors';

export const ERROR_LONG_TEXT_VIOL = 'content';
export const ERROR_LONG_TEXT_MSG = 'Your post is too long';

export const ERROR_MANY_PHOTOS_VIOL = 'content';
export const ERROR_MANY_PHOTOS_MSG = 'Your post has too many photos';

/**
 *
 * @param content
 * @returns {boolean} True when content is good!
 */
export function checkText(content) {
  const rgxTrailingSpaces = /^\s*(.+?)\s*$/g;
  const trimmed = content.replace(rgxTrailingSpaces, '$1');
  const sanitized = content.split('\n').reduce((acc, line) => {
    const sanitized = sanitizeHtml(line, { allowedTags: [] }).replace(rgxTrailingSpaces, '$1');
    acc.push(sanitized);
    return acc;
  }, []).join('\n');

  return trimmed === sanitized;
}

export function makeError(type, message) {
  return { type, message };
};