import { expect } from 'chai';

import Post from './Post';
import {
  ERROR_CONTENT_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_EMPTY_MSG,
  ERROR_EMPTY_VIOL,
  ERROR_LONG_TEXT_MSG,
  ERROR_LONG_TEXT_VIOL,
  ERROR_MANY_PHOTOS_MSG,
  ERROR_MANY_PHOTOS_VIOL
} from './validations';

describe('Post', () => {
  it('must invalidate empty post', async () => {
    const post = new Post({
      user_id: '1'
    });

    try {
      await post.save();
    } catch (errs) {
      expect(errs).to.be.an('array');
      expect(errs).to.have.length(1);
      expect(errs[0]).to.deep.equal({
        type: ERROR_EMPTY_VIOL,
        message: ERROR_EMPTY_MSG
      });
    }
  });

  it('must invalidate long post', async () => {
    const longPost = new Array(501).fill('x').join('');
    const post = new Post({
      user_id: '1',
      content: longPost
    });

    try {
      await post.save();
    } catch (errs) {
      expect(errs).to.be.an('array');
      expect(errs).to.have.length(1);
      expect(errs[0]).to.deep.equal({
        type: ERROR_LONG_TEXT_VIOL,
        message: ERROR_LONG_TEXT_MSG
      });
    }
  });

  it('must invalidate post with too many media items', async () => {
    const post = new Post({
      user_id: '1',
      media: [ 'p1.jpg', 'p2.jpg', 'p3.jpg', 'p4.jpg', 'p5.jpg', 'p6.jpg' ]
    });

    try {
      await post.save();
    } catch (errs) {
      expect(errs).to.be.an('array');
      expect(errs).to.have.length(1);
      expect(errs[0]).to.deep.equal({
        type: ERROR_MANY_PHOTOS_VIOL,
        message: ERROR_MANY_PHOTOS_MSG
      });
    }
  });

  describe('must invalidate dirty content', () => {
    [
      'Hello!<script>alert("alert!")</script>',
      'Hello!\n<script>alert("alert!")</script>',
      'Hello <p>World!</p>',
      '<h1>Header</h1>\nHello\n<p>World!</p>',
      '{}'
    ].forEach((dirtyContent) => {
      it(`content = "${dirtyContent}"`, async () => {
        const post = new Post({
          user_id: '1',
          content: dirtyContent
        });

        try {
          await post.save();
        } catch (errs) {
          expect(errs).to.be.an('array');
          expect(errs).to.have.length(1);
          expect(errs[0]).to.deep.equal({
            type: ERROR_CONTENT_VIOL,
            message: ERROR_CONTENT_MSG
          });
        }
      });
    });
  });
});