import { expect } from 'chai';

import Comment from './Comment';
import {
  ERROR_CONTENT_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_EMPTY_MSG,
  ERROR_EMPTY_VIOL,
  ERROR_LONG_TEXT_MSG,
  ERROR_LONG_TEXT_VIOL
} from './validations';

describe('Comment', () => {
  it('must invalidate empty comment', async () => {
    const comment = new Comment({
      user_id: '1'
    });

    try {
      await comment.save();
    } catch (errs) {
      expect(errs).to.be.an('array');
      expect(errs).to.have.length(1);
      expect(errs[0]).to.deep.equal({
        type: ERROR_EMPTY_VIOL,
        message: ERROR_EMPTY_MSG
      });
    }
  });

  it('must invalidate long comment', async () => {
    const longComment = new Array(501).fill('x').join('');
    const comment = new Comment({
      user_id: '1',
      content: longComment
    });

    try {
      await comment.save();
    } catch (errs) {
      expect(errs).to.be.an('array');
      expect(errs).to.have.length(1);
      expect(errs[0]).to.deep.equal({
        type: ERROR_LONG_TEXT_VIOL,
        message: ERROR_LONG_TEXT_MSG
      });
    }
  });

  describe('must invalidate dirty content', () => {
    [
      'Hello!<script>alert("alert!")</script>',
      'Hello!\n<script>alert("alert!")</script>',
      'Hello<p>World!</p>',
      '<h1>Header</h1>\nHello\n<p>World!</p>',
      '{}'
    ].forEach((dirtyContent) => {
      it(`content = "${dirtyContent}"`, async () => {
        const comment = new Comment({
          user_id: '1',
          content: dirtyContent
        });

        try {
          await comment.save();
        } catch (errs) {
          expect(errs).to.be.an('array');
          expect(errs).to.have.length(1);
          expect(errs[0]).to.deep.equal({
            type: ERROR_CONTENT_VIOL,
            message: ERROR_CONTENT_MSG
          });
        }
      });
    });
  });
});