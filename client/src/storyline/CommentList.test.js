import React from 'react';
import { makeAppStore } from '../store';
import {configure, shallow, mount, render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

const { JSDOM } = require('jsdom');

configure({adapter: new Adapter()});

import CommentList from './CommentList';

describe('CommentList', function () {
  let post;

  beforeEach(() => {
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const {window} = jsdom;
    global.window = window;
    global.document = window.document;

    post = {
      loadComments: jest.fn(() => []),
      addComment: jest.fn(() => Promise.resolve(true)),
      comments: []
    };
  });
  afterEach(() => {
    delete global.window;
    delete global.document;
  });

  it('should render pristine state', () => {
    const wrapper = render(<CommentList appStore={makeAppStore()} post={post} />);

    expect(wrapper.find('[data-test-id=comment]').val()).toBe('');
    expect(wrapper.find('[data-test-id=button-older-comments]')).toBeDefined();
    expect(wrapper.find('[data-test-id=button-post]')).toBeDefined();

    expect(post.loadComments).toHaveBeenCalled();
  });

  it('should invalidate too long a comment', () => {
    const wrapper = mount(<CommentList appStore={makeAppStore()} post={post}></CommentList>)
    const longPost = new Array(501).fill('x').join('');
    const instance = wrapper.instance().wrappedInstance;

    expect(instance.state.comment).toBe('');

    instance.onTextareaChange({ target: { value: longPost} });
    instance.onComment();
    wrapper.update();

    expect(instance.state.comment).toBe(longPost);
    expect(post.addComment).not.toHaveBeenCalledWith()

    expect(instance.state.error).toBeDefined();
    expect(instance.state.error.type).toBeDefined();
    expect(instance.state.error.message).toBeDefined();
  });

  describe('Dirty comments', () => {
    it.each([
      ['Hello! <script>alert(10000)</script>', 'Hello!', true],
      ['Hello!<p>alert!</p>', 'Hello!alert!', true]
    ])('should clear strange chars', function (whackyContent, sanitized, expected) {
      const appStore = makeAppStore();
      const wrapper = mount(<CommentList appStore={appStore} post={post} />);
      const instance = wrapper.instance().wrappedInstance;

      instance.onTextareaChange({target: {value: whackyContent}});
      instance.onComment();
      wrapper.update();

      expect(post.addComment).toHaveBeenCalledWith({ content: sanitized });
      expect(instance.state.comment).toEqual(whackyContent);
      expect(instance.state.error).toBeNull();
    });
  });
});