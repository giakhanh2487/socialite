import React, { Component } from 'react';
import { observer } from 'mobx-react';

import CommentList from './CommentList';

@observer
export default class AlbumPost extends Component {

  onCommentsClick = (e) => {
    this.props.post.isCommentsVisible = true;
  }

  render () {
    const { post } = this.props;
    const isCommentsVisible = post.isCommentsVisible;

    return (
      <div className="album-post">
        <div className="album-post__user" title={post.user.name}>
          {post.user.name}
        </div>
        <div className="album-post__content">
          <p>{post.content}</p>
        </div>
        <div className="album-post__photos">
          <div className="album-photo">
            <img alt="Click to view more" src={post.media[0]} />
          </div>
        </div>
        {!isCommentsVisible &&
        <div className="album-post__actions">
          {post.media.length > 1 &&
          <button className="btn btn-link">View Album</button>
          }
          <button className="btn btn-link" onClick={this.onCommentsClick}>Comments</button>
        </div>
        }
        {isCommentsVisible &&
        <CommentList post={post}/>
        }
      </div>
    );
  }
}
