import React, { Component } from 'react';
import { observer } from 'mobx-react';

import CommentList from './CommentList';

@observer
export default class SimplePost extends Component {
  onCommentsClick = (e) => {
    this.props.post.isCommentsVisible = true;
  }

  render () {
    const { post } = this.props;
    const isCommentsVisible = post.isCommentsVisible;

    return (
      <div className="simple-post">
        <div className="simple-post__user" title={post.user.name}>
          {post.user.name}
        </div>
        <div className="simple-post__content">
          {htmlFormat(post.content)}
        </div>
        {!isCommentsVisible &&
        <div className="simple-post__actions">
          <button className="btn btn-link" onClick={this.onCommentsClick}>Comments</button>
        </div>
        }
        {isCommentsVisible &&
        <CommentList post={post}/>
        }
      </div>
    );
  }
}

const htmlFormat = (content) => (
  <React.Fragment>
    {content.split('\n').reduce((acc, line) => {
      acc.push(<p>{line}</p>);
      return acc;
    }, [])}
  </React.Fragment>
);