import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import Dropzone from 'react-dropzone';
import {
  sanitize, makeError,
  RULE_CHARS_LIMIT,
  RULE_FILE_UPLOAD_LIMIT,
  ERROR_EMPTY_VIOL,
  ERROR_EMPTY_MSG,
  ERROR_CONTENT_VIOL,
  ERROR_CONTENT_MSG,
  ERROR_LONG_TEXT_VIOL,
  ERROR_LONG_TEXT_MSG,
  ERROR_MANY_PHOTOS_VIOL,
  ERROR_MANY_PHOTOS_MSG
} from '../validations';

import './speakbox.scss';

@inject("appStore")
@observer
export default class SpeakBox extends Component {
  static MODE_TEXT_ONLY = 'text';
  static MODE_TEXT_AND_PHOTOS = 'text-photos';

  state = {
    mode: SpeakBox.MODE_TEXT_ONLY,
    content: '',
    uploads: [],
    error: null
  };

  onTextareaChange = ({target: {value}}) => {
    this.setState({
      content: value,
      error: null
    });
  }

  onPost = () => {
    const { appStore } = this.props;
    const { content, uploads } = this.state;
    const sanitized = sanitize(content);

    let error = validate({ content: sanitized, photos: uploads });
    if (error !== false) {
      this.setState({
        error
      });
      return;
    }

    appStore.createPost({ content: sanitized, uploads }).then(() => {
      this.setState({
        content: '',
        uploads: []
      })
    });
  }

  onDropzoneDrop = (acceptedFiles) => {
    console.info('acceptedFiles:', acceptedFiles);

    this.setState(({uploads}) => ({
      uploads: uploads.concat(acceptedFiles).slice(0,5)
    }));
  }

  onRemoveUpload = (index) => {
    this.setState(({uploads}) => ({
      uploads: uploads.filter((_, i) => i !== index)
    }));
  }

  render () {
    const { mode, content, uploads, error } = this.state;
    if (error) {
      console.warn(error);
    }

    return (
      <div className="speakbox clearfix">
        <div className="speakbox__content">
          <textarea className={`form-control ${!!error && 'is-invalid'}`} cols="30" rows="3"
                    onChange={this.onTextareaChange} value={content}
                    placeholder="Speak up your mind!"
                    data-test-id="content">
          </textarea>
          {error &&
          <div className="invalid-feedback">
            {error.message}
          </div>
          }
          {mode === SpeakBox.MODE_TEXT_AND_PHOTOS &&
          <Dropzone className="speakbox__dropzone"
                    activeClassName="speakbox__dropzone--active"
                    accept="image/jpeg, image/png, image/gif"
                    onDrop={this.onDropzoneDrop}
                    disabled={uploads.length === RULE_FILE_UPLOAD_LIMIT}>
            <p>Drop your images here</p>
          </Dropzone>
          }
          {!!uploads.length &&
          <div className="speakbox__upload-previews">
            {uploads.map((item, index) =>
              <ImagePreview key={`preview-${index}`} src={item.preview}
                            onRemove={() => this.onRemoveUpload(index)}/>
            )}
          </div>
          }
        </div>
        <div className="speakbox__actions">
          <button className="btn btn-outline-secondary"
                  onClick={() => this.setState({ mode: SpeakBox.MODE_TEXT_AND_PHOTOS })}
                  data-test-id="button-photos">Photos</button>
          <button className="btn btn-primary float-right" onClick={this.onPost}
                  disabled={!!error}
                  data-test-id="button-post">POST</button>
        </div>
      </div>
    );
  }
}

const ImagePreview = ({ src, onRemove }) => (
  <div className="image-preview" style={{backgroundImage: `url(${src})`}}>
    <div className="btn btn-link" onClick={onRemove}><span role="img" aria-label="Preview">&#x274E;</span></div>
  </div>
);

/**
 * Validation execution
 */
const validate = ({ content, photos }) => {
  // required
  if (!content && !(photos && photos.length)) {
    return makeError(ERROR_EMPTY_VIOL, ERROR_EMPTY_MSG);
  }

  // Length
  if (content.length > RULE_CHARS_LIMIT) {
    return makeError(ERROR_LONG_TEXT_VIOL, ERROR_LONG_TEXT_MSG);
  }
  // Content issues
  if (!(/^[\w\s!@#$%^&*():;"'<=>+.,?/]*$/gm.test(content))) {
    return makeError(ERROR_CONTENT_VIOL, ERROR_CONTENT_MSG);
  }

  if (photos.length > RULE_FILE_UPLOAD_LIMIT) {
    return makeError(ERROR_MANY_PHOTOS_VIOL, ERROR_MANY_PHOTOS_MSG);
  }

  return false;
};