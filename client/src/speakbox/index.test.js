import React from 'react';
import { makeAppStore } from '../store';
import {configure, shallow, mount, render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

const { JSDOM } = require('jsdom');

configure({adapter: new Adapter()});

import SpeakBox from './index';

describe('Speakbox', function () {
  beforeEach(() => {
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
  });
  afterEach(() => {
    delete global.window;
    delete global.document;
  });

  it('should render pristine state', function () {
    const wrapper = render(<SpeakBox appStore={makeAppStore()} />);
    expect(wrapper.find('[data-test-id=content]').val()).toBe('');
    expect(wrapper.find('[data-test-id=button-photos]')).toBeDefined();
    expect(wrapper.find('[data-test-id=button-post]')).toBeDefined();
  });

  it('should invalidate empty', function () {
    const wrapper = mount(<SpeakBox appStore={makeAppStore()} />);
    expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBeFalsy();
    expect(wrapper.find('.invalid-feedback').length).toEqual(0);

    wrapper.find('[data-test-id="button-post"]').simulate('click');
    expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBeTruthy();
    expect(wrapper.find('.invalid-feedback').length).toEqual(1);
  });

  it('should invalidate too long posts', function () {
    const wrapper = mount(<SpeakBox appStore={makeAppStore()} />);
    expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBeFalsy();
    expect(wrapper.find('.invalid-feedback').length).toEqual(0);

    const longPost = new Array(501).fill('x').join('');
    wrapper.find('[data-test-id="content"]').prop('onChange')({target: {value: longPost}});
    wrapper.find('[data-test-id="button-post"]').simulate('click');

    expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBeTruthy();
    expect(wrapper.find('.invalid-feedback').length).toEqual(1);
  });

  describe('Dirty posts', () => {
    it.each([
      ['Hello! <script>alert(10000)</script>', 'Hello!', true],
      ['Hello! <p>alert!</p>', 'Hello! alert!', true]
    ])('should clear strange chars', function (whackyPost, sanitized, expected) {
      const appStore = makeAppStore();
      jest.spyOn(appStore, 'createPost').mockImplementation(({ content }) => {
        expect(content).toEqual(sanitized);

        return Promise.resolve({});
      });

      const wrapper = mount(<SpeakBox appStore={appStore} />);
      const instance = wrapper.instance().wrappedInstance;

      instance.onTextareaChange({target: {value: whackyPost}});
      instance.onPost();

      expect(appStore.createPost).toHaveBeenCalledWith({ content: sanitized, uploads: [] });
      expect(instance.state.content).toEqual(whackyPost);
      expect(instance.state.error).toBeNull();
    });
  });

  describe('Posts with only photo(s)', () => {
    it.each([
      [[{preview: 'tmp.jpg'}], false],
      [[{preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}], false],
      [[{preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}], false],
      [[{preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}], false],
      [[{preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}, {preview: 'tmp.jpg'}], false]
    ])(
      'should allow photos only post: %j -> %s', (function (uploadedFiles, expected) {
        const appStore = makeAppStore();
        jest.spyOn(appStore, 'createPost').mockImplementation(({uploads}) => {
          expect(!uploads || uploads.length <= 5).toBeTruthy();

          return Promise.resolve({});
        });

        const wrapper = mount(<SpeakBox appStore={appStore}/>);

        expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBeFalsy();
        expect(wrapper.find('.invalid-feedback').length).toEqual(0);

        wrapper.find('[data-test-id="button-photos"]').simulate('click');
        expect(wrapper.find('.speakbox__upload-previews').length).toEqual(0);

        wrapper.instance().wrappedInstance.onDropzoneDrop(uploadedFiles);
        wrapper.update();

        expect(wrapper.find('.speakbox__upload-previews').length).toEqual(1);

        wrapper.find('[data-test-id="button-post"]').simulate('click');
        wrapper.update();

        expect(appStore.createPost).toHaveBeenCalled();
        expect(wrapper.find('[data-test-id="button-post"]').prop('disabled')).toBe(expected);
        expect(wrapper.find('.invalid-feedback').length).toEqual(0);
      }));
  });
});
